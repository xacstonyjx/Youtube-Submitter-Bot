Post Youtube Videos to Subreddit
================================

Installations
-------------

This script requires python 3, praw, praw-oauth2util, and the google python api.

To install praw type in your command line:

    pip install praw
    pip install praw-oauth2util

To install the google api type in your command line:

    pip install google-api-python-client

*Note: the find_channel_id.py file is an optional utility. See the Getting Channel IDs For Searching section for more info*

Reddit OAuth Setup
------------------

* [Go here on the account you want the bot to run on](https://www.reddit.com/prefs/apps/)
* Click on create a new app.
* Give it a name. 
* Select script from the radio buttons.
* Set the redirect uri to `http://127.0.0.1:65010/authorize_callback`
* After you create it you will be able to access the app key and secret.
* The **app key** is found here. Add this to the app_key variable in the oauth.txt file.

![here](http://i.imgur.com/7ybI5Fo.png) (**Do not give out to anybody**) 

* And the **app secret** here. Add this to the app_secret variable in the oauth.txt file.

![here](http://i.imgur.com/KkPE3EV.png) (**Do not give out to anybody**)

Note: On the first run of the bot you'll need to run it on a system that has a desktop environment.
So, if you're planning on running the bot on a server or VPS somewhere, run it on your computer
first. The first run will require you to authenticate it with the associated Reddit account by
opening a web page that looks like this:

![authentication page](http://i.imgur.com/se53uTq.png)

It will list different things it's allowed to do depending on the bots scope. After you
authenticate it, that page won't pop up again unless you change the OAuth credentials. And you'll
be free to run it on whatever environment you choose.

Setting Up Your Google Dev Account
----------------------------------

Then you must get your developer key from google, to do that go to:

[console.developers.google.com/project](https://console.developers.google.com/project)

And create a project.

From the console now, go to APIs & auth, and navigate to the APIs screen.

Click on YouTube Data API

![Youtube data api](http://i.imgur.com/jN79VWq.png)

And click enable API at the top.

![Activate Youtube API](http://i.imgur.com/9LvPHgQ.png)

From there click on credentials and you'll see in the Public API Access area
and line called API key, copy that into the variable DEVELOPER_KEY in
youtube_submitter_bot.py.

![Developer key](http://i.imgur.com/MR9recA.png)

Getting Channel IDs For Searching
---------------------------------

To get the channel IDs for the bot, you can either use the inspect element on your browser
or run the find_channel_id.py file included in the repo. Valid URLs for getting channel IDs
are either a video page of the channel you want the ID for, or the channel's page itself.

##### Using the find_channel_id.py script

You have two options when running the script, you can either enter urls indvidually and get the
channel ID returned, or save them all in a .txt file and have all the channel IDs returned at once.

The first option, you'll just enter 1 after starting the script, and then paste in the url of the video
page or channel page of the channel you need the ID for. And then the channel ID will be printed on the screen.

The second option, you'll enter 2, and it will ask for you the filename. Enter the filename you have saved
of urls that is in the same directory as the script (e.g. urls.txt). The urls must each been on there own line
like:

    https://www.youtube.com/channel/channel1
    https://www.youtube.com/watch?v=videoforchannel2
    etc.

And it will print out a list of all the channel IDs.

##### Using inspect element in your browser

Go to the video or channel page of a video posted by the channel you want. Right click, and inspect element.
In the inspect element hit CTRL+F and type "data-channel-external-id" which will take you to something that
looks like `data-channel-external-id="UCjXfkj5iapKHJrhYfAF9ZGg"`. The long letters and numbers in quotes is the
channel id. Paste that (quotes included) in between the brackets [ ] for the `CHANNEL_IDS`.


Config
------

You can set the WAIT_TIME variable to how long you would like to bot to wait in between cycles
of video fetching. The time is in seconds and the default is 3600 (1 hour). Set this
to `False` or `0` if you don't want it to wait and just exit after each run. This is
if you want to set it to run via Task Scheduler, Crontab, or run it manually yourself.

You can set the MAX_RESULTS variable for it to fetch that many variables. Default is 1,
and it will fetch the newest video each time. If you need it to fetch more you can
set it higher.

If you would like, you can have it add the channel name in brackets to the title of the post by
setting `ADD_CHANNEL_NAME_TO_TITLE` to `True`. The title will then be:

    [Channel Name] Video Title

If you would like to have the bot flair your posts set `ADD_LINK_FLAIR_TO_POSTS` to `True`. Then you will 
need to configure the link flair. Set `APPEND_CHANNEL_TO_CSS_CLASS` to add the channel name to the link flair's
css class if you want custom styling for each channel. Set the `LINK_FLAIR_CSS_CLASS` to whatever you
want the class for the videos to be. If you are appending the channel name to the class, it will get added on
to the end of the class with a hypen like so:

    CSSClass-ChannelName

Then you can set a static link flair text like 'Video' by setting `LINK_FLAIR_TEXT` or have the bot set the 
link flair text to the channel name by `CHANNEL_NAME_AS_FLAIR_TEXT` to `True`. Note if you have 
`CHANNEL_NAME_AS_FLAIR_TEXT` set to `True` and text in the `LINK_FLAIR_TEXT`, it will default to the
`CHANNEL_NAME_AS_FLAIR_TEXT`.

License
-------

The MIT License (MIT)

Copyright (c) 2015 Nick Hurst

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
