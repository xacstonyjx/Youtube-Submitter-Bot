import re
import requests

OPTIONS = """
Welcome to the youtube channel id finder.
1. Find by video or channel url
2. Batch find from txt file
3. Exit
"""


def find_channel_id(url):
    try:
        request = requests.get(url)
        request.raise_for_status()  # raise an error for bad request
    except requests.exceptions.MissingSchema:
        return 'Couldn\'t find, invalid url'
    except requests.exceptions.HTTPError as e:
        return 'Couldn\'t make request because: {}'.format(e)

    regex = r'(?<=data-channel-external-id=")([\s\S])\w+'
    search = re.search(regex, request.text)
    if search:
        return search.group(0)
    else:
        return 'Couldn\'t find channel ID, check the url or try another'


def batch_find_ids(fname):
    try:
        with open(fname, 'r') as f:
            urls = f.read().splitlines()
    except FileNotFoundError:
        print('ERROR: Couldn\'t find file')
        return

    c_ids = []
    for url in urls:
        c_ids.append(find_channel_id(url))

    print('Here are your Channel IDs:')
    print(c_ids)


def main():
    print(OPTIONS)
    while True:
        choice = input('What would you like to do? ')
        if choice is '1':
            url = input('Enter the url: ')
            c_id = find_channel_id(url)
            print('Channel ID: {}'.format(c_id))
        elif choice is '2':
            fname = input('Enter the name of the file: ')
            batch_find_ids(fname)
        else:
            exit(0)

if __name__ == '__main__':
    main()
